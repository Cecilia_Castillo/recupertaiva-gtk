import gi
gi.require_version("Gtk", "3.0")

from gi.repository import Gtk


class principal:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("ventana.glade")

        ventana = builder.get_object("ventana")
        ventana.connect("destroy", Gtk.main_quit)

        self.numero1 = 0
        self.numero2 = 0
        self.numero3 = 0
        self.numero4 = 0

        self.entry1 = builder.get_object("entry1")
        self.entry2 = builder.get_object("entry2")
        self.entry3 = builder.get_object("entry3")
        self.entry4 = builder.get_object("entry4")
        self.label1 = builder.get_object("label1")
        self.label2 = builder.get_object("label2")
        self.label3 = builder.get_object("label3")
        self.label4 = builder.get_object("label4")

        self.boton_aceptar = builder.get_object("botonaceptar")
        self.boton_reset = builder.get_object("botonreset")

        self.boton_aceptar.connect("clicked", self.calcular)
        self.boton_reset.connect("clicked", self.reset)
        self.entry1.connect("activate", self.actualizar)
        self.entry1.connect("changed", self.actualizar)
        self.entry2.connect("activate", self.actualizar)
        self.entry2.connect("changed", self.actualizar)
        self.entry3.connect("activate", self.actualizar)
        self.entry3.connect("changed", self.actualizar)
        self.entry4.connect("activate", self.actualizar)
        self.entry4.connect("changed", self.actualizar)

        # mostrar la ventana
        ventana.show_all()

    def actualizar(self, dsasdasd=None):
        texto1 = self.entry1.get_text()
        texto2 = self.entry2.get_text()
        texto3 = self.entry3.get_text()
        texto4 = self.entry4.get_text()

        if texto1.isnumeric():
            self.numero1 = int(texto1)
        else:
            self.numero1 = len(texto1)

        if texto2.isnumeric():
            self.numero2 = int(texto2)
        else:
            self.numero2 = len(texto2)

        if texto3.isnumeric():
            self.numero3 = int(texto3)
        else:
            self.numero3 = len(texto3)

        if texto4.isnumeric():
            self.numero4 = int(texto4)
        else:
            self.numero4 = len(texto4)

        self.actualizar_labels()

    def actualizar_labels(self):
        self.label1.set_text(str(self.numero1))
        self.label2.set_text(str(self.numero2))
        self.label3.set_text(str(self.numero3))
        self.label4.set_text(str(self.numero4))

    def calcular(self, fdsfd=None):
        suma = self.numero1 + self.numero2 + self.numero3 + self.numero4
        print("calcular, = " + str(suma))
        dialog = Gtk.MessageDialog(type=Gtk.MessageType.INFO, buttons=Gtk.ButtonsType.CLOSE)
        texto = "\nlos datos ingresados son:\n"
        texto += str(self.numero1) + " + " + str(self.numero2) + " + " + str(self.numero3) + " + " + str(self.numero4)
        texto += "\n que resulta"
        texto += "\n = " + str(suma)

        dialog.format_secondary_text(texto)

        def response(dialogo, response):
            dialog.close()

        dialog.connect("response", response)
        dialog.run()

    def reset(self, asdsasd=None):
        dialog = Gtk.MessageDialog(type=Gtk.MessageType.QUESTION, buttons=Gtk.ButtonsType.YES_NO)
        dialog.format_secondary_text("esta seguro de que desea borrar los datos ingresados?")

        def response(dialogo, response):
            dialog.close()

            if response == Gtk.ResponseType.YES:
                self.borrar_datos()

        dialog.connect("response", response)
        dialog.run()

    def borrar_datos(self):
        self.entry1.set_text("")
        self.entry2.set_text("")
        self.entry3.set_text("")
        self.entry4.set_text("")
        self.numero1 = 0
        self.numero2 = 0
        self.numero3 = 0
        self.numero4 = 0
        self.actualizar_labels()


if __name__ == "__main__":
    w = principal()
    Gtk.main()
